# Tutorial Sphinx

A continuación, se presentará una serie de pasos y recursos necesarios
para la ejecución de la herramienta Sphinx la cual genera documentation
automáticamente a partir de los Docstring de Python. 

Estos pasos están diseñados para ser ejecutados en Linux, no han sido probados
en ninguna version Windows.

En este caso, la autodocumentación fue creada para un proyecto simple creado
en FastAPI. https://fastapi.tiangolo.com/

### _**_1) Instalar Sphinx en Linux:_**_

**sudo apt-get install python3-sphinx**

### _**_2) Temas Sphinx:_**_
   
Sphinx cuenta con varios temas, los cuales pueden ser encontrados en el siguiente enlace
https://sphinx-themes.org/
   
En este caso, fue usado el sphinx rtd theme. Para Instalar:

**pip install sphinx_rtd_theme**

### _**_3) Crear carpeta para guardar la documentación:_**_
Dentro de la carpeta principal del proyecto, crear una carpeta llamada **docs**. 
En esta carpeta se guardaran los archivos HTML que se generaran automáticamente con Sphinx.

### _**_4) Iniciar la configuración de Sphinx:_**_

Ejecutar en consola **estando adentro de la carpeta anteriormente creada (docs)**

**sphinx-quickstart**

Esto arrojará una serie de preguntas las cuales se deben ir respondiendo, pregunta
por el lenguaje del proyecto (Español, Ingles), CopyRight, Fecha de lanzamiento, etc.
 
En el siguiente enlace se puede ver como configurar cada idioma https://www.sphinx-doc.org/es/1.6/config.html

### _**_5) Editar el archivo conf.py:_**_

Una vez ejecutado el anterior comando, automáticamente Sphinx creará dentro de la carpeta **docs**
varios archivos y carpetas, en los cuales se encuentra uno llamado **conf.py**

Una vez abierto el archivo **conf.py**, las lineas (aproximadamente) 13, 14 y 15 aparecerían
comentadas por default, se deben des comentar y quedará algo como lo siguiente:

    import os
    import sys
    sys.path.insert(0, os.path.abspath('..'))

Notar que en la última linea (sys.path.insert.....) el parámetro que recibe el método **.abspath**
fue modificado y en vez de tener 1 solo punto tal y como viene por default, se modificó por
2 puntos **abspath('..')**, esto nos indica que los modulos a ser documentados no están dentro
de la carpeta docs sino en la anterior a ella, es decir en la raiz del proyecto.

Aproximadamente en la línea 33 del archivo **conf.py** se encuentra una variable llamada
**extensions** la cual debe ser reemplazada por la siguiente linea 

    extensions = ['sphinx.ext.todo', 'sphinx.ext.viewcode', 'sphinx.ext.autodoc']

Cambiar la variable **html_theme** la cual se encuentra en la línea 40 aproximadamente por

    html_theme = 'sphinx_rtd_theme'

Esto es opcional y es solo para cambiar el tema que viene por default en Sphinx.

### _**_6) Editar el archivo index.rst:_**_
 
Dentro de la carpeta **docs** buscar el archivo **index.rst**, precisamente
las líneas 9 a 11 aproximadamente y agregar la palabra **modules**. Ejemplo:

    .. toctree::
    :maxdepth: 2
    :caption: Contents:

    modules

**IMPORTANTE**: Notar que la palabra **modules** está separada por un espacio respecto a la línea **:caption: Contents:**

### _**_7) Generar archivos HTML con Sphinx:_**_

Finalmente, copiar en consola (estando adentro de la carpeta **docs**) el comando

**sphinx-apidoc -o . ..** 

Posteriormente, copiar en consola el comando

**make html**


Una vez realizados los anteriores pasos, ingresar a la ruta docs/_build/html y ejecutar el archivo
index.html, el cual abrirá en el navegador la documentación generada a partir de los Docstring escritos en Python.








   











