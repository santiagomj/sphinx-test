#  uvicorn main:app --reload
#  http://127.0.0.1:8000/docs
#  http://127.0.0.1:8000/redoc

from fastapi import FastAPI
from pydantic import BaseModel

app = FastAPI()


@app.get("/")
def read_root():
    """
    Endpoint para imprimir hello world

    :return: Diccionario
    """
    return {"Hello": "World"}


@app.get("/items/{item_id}")
def read_item(item_id: int, q: str = None):
    """
    :param item_id: id del item ingresado desde la url
    :type item_id: Int

    :param q: Valor opcional para X funcionalidad, None por default
    :type q: String

    :return: Diccionario
    """
    return {"item_id": item_id, "q": q}
